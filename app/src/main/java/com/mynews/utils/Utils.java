package com.mynews.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Utils {
    public static List<Integer> getRandomNumList(int nums,int start,int end){
        List<Integer> list = new ArrayList<>();
        Random r = new Random();
        while(list.size() != nums){
            int num = r.nextInt(end-start) + start;
            if(!list.contains(num)){
                list.add(num);
            }
        }
        return list;
    }
}