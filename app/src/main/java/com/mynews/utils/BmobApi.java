package com.mynews.utils;

import com.mynews.activities.App;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import okhttp3.Callback;
import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.TlsVersion;

public class BmobApi {
    private static final String id="bf3e46c00ff857bd08d0c6c51b0c7b8e";
    private static final String key="6e9d07da2f864134b57cafd61a558892";
    private static final String mkey="a900861474d720ffc4f249d140c0a3b9";
    public static Request.Builder getBuilder(String url){
        return new Request.Builder().url(url).addHeader("X-Bmob-Application-Id",id).addHeader("X-Bmob-REST-API-Key",key).addHeader("X-Bmob-Master-Key",mkey).addHeader("Content-Type","application/json");
    }
    public static OkHttpClient getClient(){
        return new OkHttpClient.Builder()
                .readTimeout(3000, TimeUnit.SECONDS)
                .connectTimeout(3000, TimeUnit.SECONDS)
                .writeTimeout(3000,TimeUnit.SECONDS)
                .followSslRedirects(true)
                .build();
    }
    public static void get(String url, Callback callback){
      getClient().newCall(getBuilder(url).build()).enqueue(callback);
    }
    public static void post(JSONObject jsonObject,String url,Callback callback){
        getClient().newCall(getBuilder(url).post(RequestBody.create(MediaType.parse("application/json;charset=utf-8"), String.valueOf(jsonObject))).build()).enqueue(callback);
    }
    public static void put(JSONObject data,String url,boolean isPutSession,Callback callback){
        if (isPutSession){
            getClient().newCall(getBuilder(url).addHeader("X-Bmob-Session-Token", App.user.sessionToken).put(RequestBody.create(MediaType.parse("application/json;charset=utf-8"), String.valueOf(data))).build()).enqueue(callback);
        }else {
            getClient().newCall(getBuilder(url).put(RequestBody.create(MediaType.parse("application/json;charset=utf-8"), String.valueOf(data))).build()).enqueue(callback);
        }
    }
    public static void delete(String url,Callback callback){
        getClient().newCall(getBuilder(url).delete().build()).enqueue(callback);
    }
}