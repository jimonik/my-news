package com.mynews.activities;

import android.app.Application;
import android.content.SharedPreferences;

import com.mynews.bean.NewsBean;
import com.mynews.bean.User;

public class App extends Application {
    public static User user;
    public static SharedPreferences sp;
    public static NewsBean newsBean;
    @Override
    public void onCreate() {
        super.onCreate();
        sp=getSharedPreferences("data",MODE_PRIVATE);
    }

    public static int getTypeByName(String name) {
        switch (name){
            case "娱乐":return 1;
            case "疫情":return 2;
            case "热点":return 3;
            case "生活":return 4;
            case "体育":return 5;
            case "科技":return 6;
            case "新时代":return 7;
            default:return 8;
        }
    }
    public static String getNameByType(int i) {
        switch (i){
            case 1:return  "娱乐";
            case 2:return  "疫情";
            case 3:return  "热点";
            case 4:return  "生活";
            case 5:return  "体育";
            case 6:return  "科技";
            case 7:return "新时代";
            default:return "其他";
        }
    }
}
