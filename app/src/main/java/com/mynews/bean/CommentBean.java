package com.mynews.bean;

import android.app.Activity;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import com.mynews.utils.BmobApi;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
public class CommentBean {
    public boolean isComment;
    public String id,ownerId,content,ifReplyCommentId;
    public CommentBean(boolean isComment, String id, String ownerId, String content,String ifReplyCommentId){
        this.isComment=isComment;
        this.ownerId=ownerId;
        this.id=id;
        this.content=content;
        this.ifReplyCommentId=ifReplyCommentId;
    }
    @BindingAdapter(value = {"bindName","activity"}, requireAll = false)
    public static void getName(TextView textView, String uid, Activity activity){
        BmobApi.get("https://api2.bmob.cn/1/users/"+uid, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                activity.runOnUiThread(() -> textView.setText("未知用户"));
            }
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
             activity.runOnUiThread(() -> {
                 try {
                     assert response.body() != null;
                     JSONObject jo = new JSONObject(response.body().string());
                     if (jo.isNull("error")){
                         textView.setText(jo.getString("nickname"));
                     }else {
                         textView.setText("未知用户");
                     }
                 } catch (Exception e) {
                     e.printStackTrace();
                 }
             });
            }
        });
    }
}