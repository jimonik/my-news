package com.mynews.bean;
public class User {
    public String email,sessionToken,objectId,username,nickname;
    public boolean isAdmin;
    public User(String email, String sessionToken, String objectId, String username, String nickname,boolean isAdmin){
        this.email=email;
        this.sessionToken=sessionToken;
        this.objectId=objectId;
        this.username=username;
        this.nickname=nickname;
        this.isAdmin=isAdmin;
    }
}
