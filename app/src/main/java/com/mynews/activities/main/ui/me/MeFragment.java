package com.mynews.activities.main.ui.me;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.library.baseAdapters.BR;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.mynews.R;
import com.mynews.databinding.FragmentMeBinding;

public class MeFragment extends Fragment {
    private MeVM vm;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_me, container, false);
        FragmentMeBinding binding = DataBindingUtil.bind(root);
        vm = new ViewModelProvider(this).get(MeVM.class);
        binding.setVariable(BR.vm,vm);
        binding.setLifecycleOwner(this);
        vm.setBinding(binding,this,requireActivity());
        return root;
    }
    @Override
    public void onResume() {
        super.onResume();
        vm.onResume();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        vm.onActivity();
    }
}