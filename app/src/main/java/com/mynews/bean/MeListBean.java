package com.mynews.bean;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

public class MeListBean {
    public  int iconId;
public String text;
    public MeListBean(int iconId, String text){
        this.iconId=iconId;
        this.text=text;
    }
    @BindingAdapter("bindImg")
    public static void bindImg(ImageView imageView,int id){
        imageView.setImageResource(id);
    }
}
