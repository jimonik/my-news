package com.mynews.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mynews.R;
import com.mynews.bean.NewsBean;
import com.mynews.bean.User;
import com.mynews.utils.BmobApi;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class NewsAdapter extends ArrayAdapter<NewsBean> {
    private static final int layoutId = R.layout.item_news;
    @SuppressLint("StaticFieldLeak")
    public static Activity content;
    public NewsAdapter(Activity context, List<NewsBean> objects){
        super(context,layoutId,objects);
        content=context;
    }
    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View view;
        ViewHolder viewHolder = new ViewHolder();
        if (convertView==null){
            view=LayoutInflater.from(getContext()).inflate(layoutId,parent,false);
            view.setTag(viewHolder);
        } else{
            view=convertView;
        }
        viewHolder.title=view.findViewById(R.id.item_news_title);
        viewHolder.content=view.findViewById(R.id.item_news_content);
        viewHolder.source=view.findViewById(R.id.item_news_source);
        viewHolder.time=view.findViewById(R.id.item_news_time);
        final NewsBean newsBean = getItem(position);
        if (newsBean.user==null&&!newsBean.ownerId.equals("")){
            BmobApi.get("https://api2.bmob.cn/1/users/" + newsBean.ownerId, new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) { }
                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) {
                  content.runOnUiThread(() -> {
                      try {
                          assert response.body() != null;
                          JSONObject jo = new JSONObject(response.body().string());
                          newsBean.user=new User(jo.getString("email"),null,jo.getString("objectId"),jo.getString("username"),jo.getString("username"),jo.getBoolean("isAdmin"));
                          viewHolder.source.setText("来自:"+newsBean.user.nickname);
                          viewHolder.content.setText(newsBean.content);
                          viewHolder.title.setText(newsBean.title);
                          viewHolder.time.setText(newsBean.time);
                      } catch (JSONException | IOException e) {
                          e.printStackTrace();
                      }
                  });
                }
            });
        }else if (!newsBean.id.equals("")&&newsBean.content.equals("")){
            BmobApi.get("https://api2.bmob.cn/1/classes/news/" + newsBean.id, new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) { }
                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) {
                    try {
                        assert response.body() != null;
                        JSONObject jo = new JSONObject(response.body().string());
                        BmobApi.get("https://api2.bmob.cn/1/users/" + jo.getString("ownerId"), new Callback() {
                            @Override
                            public void onFailure(@NotNull Call call, @NotNull IOException e) { }
                            @Override
                            public void onResponse(@NotNull Call call, @NotNull Response response) {
                                content.runOnUiThread(() -> {
                                    try {
                                        assert response.body() != null;
                                        JSONObject jo1 = new JSONObject(response.body().string());
                                        newsBean.user=new User(jo1.getString("email"),null,jo1.getString("objectId"),jo1.getString("username"),jo1.getString("username"),jo1.getBoolean("isAdmin"));
                                        newsBean.content=jo.getString("content");
                                        newsBean.title=jo.getString("title");
                                        newsBean.time=jo.getString("updatedAt");
                                        viewHolder.source.setText("来自:"+newsBean.user.nickname);
                                        viewHolder.content.setText(jo.getString("content"));
                                        viewHolder.title.setText(jo.getString("title"));
                                        viewHolder.time.setText(jo.getString("updatedAt"));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                });
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }else{
            assert newsBean.user != null;
            viewHolder.source.setText("来自:"+newsBean.user.nickname);
            viewHolder.content.setText(newsBean.content);
            viewHolder.title.setText(newsBean.title);
            viewHolder.time.setText(newsBean.time);
        }
        return view;
    }
    static class ViewHolder{
        TextView title,content,source,time;
    }
}