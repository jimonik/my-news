package com.mynews.activities.like;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.mynews.R;
import com.mynews.databinding.ActivityLikeBinding;

public class LikeActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityLikeBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_like);
        LikeVM vm = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(getApplication())).get(LikeVM.class);
        binding.setVm(vm);
        binding.setLifecycleOwner(this);
        vm.setBinding(binding,this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (android.R.id.home==item.getItemId()){ finish(); }
        return super.onOptionsItemSelected(item);
    }
}
