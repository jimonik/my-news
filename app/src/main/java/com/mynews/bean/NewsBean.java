package com.mynews.bean;

public class NewsBean {
    public String id,title,content,ownerId,time,type;
    public User user;
    public NewsBean(String id, String title, String content,String ownerId,String time,String type,User user){
        this.id=id;
        this.title=title;
        this.content=content;
        this.ownerId=ownerId;
        this.user=user;
        this.type=type;
        this.time=time;
    }
}
