package com.mynews.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.mynews.BR;
import com.mynews.R;
import com.mynews.bean.CollectionBean;
import java.util.List;
import java.util.Objects;
public class CollectionAdp extends BaseAdapter {
    Context context;
    public List<CollectionBean> data;
    public CollectionAdp(Context context, List<CollectionBean> objects){
        super();
        this.context=context;
        data=objects;
    }
    @Override
    public int getCount() {
        return Objects.requireNonNull(data).size();
    }
    @Override
    public Object getItem(int position) {
        return Objects.requireNonNull(data).get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        @SuppressLint("ViewHolder") ViewDataBinding binding= DataBindingUtil.inflate(LayoutInflater.from(context),R.layout.item_collection, parent, false);
        binding.setVariable(BR.bean, Objects.requireNonNull(data).get(position));
        binding.setVariable(BR.activity,context);
        return binding.getRoot();
    }
}