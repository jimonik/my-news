package com.mynews.activities.main;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.mynews.R;
import com.mynews.activities.post.PostActivity;
import com.mynews.activities.search.SearchActivity;
import com.mynews.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        MainVM vm = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(getApplication())).get(MainVM.class);
        binding.setVm(vm);
        binding.setLifecycleOwner(this);
        vm.setBinding(binding,this);

        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},1);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        menu.getItem(0).setOnMenuItemClickListener(item -> {
            startActivity(new Intent(MainActivity.this, PostActivity.class));
            return false;
        });
        menu.getItem(1).setOnMenuItemClickListener(item -> {
            startActivity(new Intent(MainActivity.this, SearchActivity.class));
            return false;
        });
        return super.onCreateOptionsMenu(menu);
    }
}