package com.mynews.activities.main.ui.me;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;

import androidx.databinding.library.baseAdapters.BR;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mynews.R;
import com.mynews.activities.App;
import com.mynews.activities.account.AccountActivity;
import com.mynews.activities.collection.CollectionActivity;
import com.mynews.activities.comment.CommentActivity;
import com.mynews.activities.like.LikeActivity;
import com.mynews.activities.login.LoginActivity;
import com.mynews.activities.mynews.MyNewsActivity;
import com.mynews.activities.tipoff.TipOffActivity;
import com.mynews.adapter.MeListAdp;
import com.mynews.bean.MeListBean;
import com.mynews.databinding.FragmentMeBinding;

import java.util.ArrayList;
import java.util.List;


public class MeVM extends ViewModel {
    public static MutableLiveData<String> nickname=new MutableLiveData<>(),email=new MutableLiveData<>();
    private final List<MeListBean> data=new ArrayList<>();
    @SuppressLint("StaticFieldLeak")
    private static FragmentActivity fragmentActivity;
    public MeVM(){ nickname.setValue("未知用户");email.setValue("获取用户信息失败"); }
    @SuppressLint("NonConstantResourceId")
    public void setBinding(FragmentMeBinding binding, MeFragment meFragment, FragmentActivity fragmentActivity) {
        MeVM.fragmentActivity =fragmentActivity;
        data.add(new MeListBean(R.drawable.comment,"我的评论"));
        data.add(new MeListBean(R.drawable.collect,"我的收藏"));
        data.add(new MeListBean(R.drawable.like,"我赞过的"));
        data.add(new MeListBean(R.drawable.post,"我发布的"));
        data.add(new MeListBean(R.drawable.tipoff,"历史举报"));
        data.add(new MeListBean(R.drawable.exit,"退出登陆"));
        binding.setVariable(BR.adp,new MeListAdp(fragmentActivity,data));
        binding.meList.setOnItemClickListener((parent, view, position, id) -> {
            switch (data.get(position).iconId){
                case R.drawable.comment:
                    fragmentActivity.startActivity(new Intent(fragmentActivity, CommentActivity.class));
                    break;
                case R.drawable.collect:
                    fragmentActivity.startActivity(new Intent(fragmentActivity, CollectionActivity.class));
                    break;
                case R.drawable.like:
                    fragmentActivity.startActivity(new Intent(fragmentActivity, LikeActivity.class));
                    break;
                case R.drawable.post:
                    fragmentActivity.startActivity(new Intent(fragmentActivity, MyNewsActivity.class));
                    break;
                case R.drawable.tipoff:
                    fragmentActivity.startActivity(new Intent(fragmentActivity, TipOffActivity.class));
                    break;
                case R.drawable.exit:
                    SharedPreferences.Editor edit = App.sp.edit();
                    edit.putBoolean("autoLogin",false);//注册登录后都可以自动登录
                    edit.apply();
                    fragmentActivity.startActivity(new Intent(fragmentActivity, LoginActivity.class));
                    break;
            }
        });
        updateText();
    }
    public static void msg_click(View view){
         fragmentActivity.startActivityForResult(new Intent(fragmentActivity, AccountActivity.class),2);
    }
    public void onActivity() {
     updateText();
    }
    public void onResume() {
      updateText();
    }
    public void updateText(){
        if (App.user!=null){
            nickname.setValue(App.user.nickname);
            email.setValue(App.user.email);
        }else {
            nickname.setValue("未知用户");
            email.setValue("获取用户信息失败");
        }
    }
}
