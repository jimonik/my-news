package com.mynews.activities.main.ui.recommend;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.library.baseAdapters.BR;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;

import com.mynews.R;
import com.mynews.activities.view.PullingLayout;
import com.mynews.databinding.FragmentRecommendBinding;

public class RecommendFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_recommend, container, false);
        FragmentRecommendBinding binding = DataBindingUtil.bind(root);
        RecommendVM vm = new ViewModelProvider(this).get(RecommendVM.class);
        assert binding != null;
        binding.setVariable(BR.vm, vm);
        binding.setLifecycleOwner(this);
        vm.setBinding(binding, requireActivity());
        return root;
    }
}