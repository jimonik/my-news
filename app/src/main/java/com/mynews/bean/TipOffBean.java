package com.mynews.bean;

public class TipOffBean {
    public NewsBean newsBean;
    public String userId,newsId,content,time,nickname,id,process;
    public TipOffBean(String id,String userId, String newsId, String content,String time,String nickname,NewsBean newsBean,String process){
        this.id=id;
        this.userId=userId;
        this.newsId=newsId;
        this.content=content;
        this.time=time;
        this.nickname=nickname;
        this.newsBean=newsBean;
        this.process=process;
    }
}
