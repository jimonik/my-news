package com.mynews.activities.post;

import android.annotation.SuppressLint;
import android.app.Application;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.mynews.activities.App;
import com.mynews.utils.BmobApi;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
public class PostVM extends AndroidViewModel {
    public static MutableLiveData<String> title=new MutableLiveData<>(),content=new MutableLiveData<>();
    public static MutableLiveData<String[]> sp=new MutableLiveData<>();
    @SuppressLint("StaticFieldLeak")
    private static PostActivity postActivity;
    private static String nowType;
    public PostVM(@NonNull Application application) {
        super(application);
        title.setValue("");
        content.setValue("");
        sp.setValue(new String[]{"娱乐","疫情","热点","生活","体育","科技","新时代","其他"});
    }
    public void post() {
        if (!title.getValue().equals("")&&!content.getValue().equals("")){
            JSONObject jo=new JSONObject();
            try {
                jo.put("title",title.getValue());
                jo.put("content",content.getValue());
                jo.put("ownerId", App.user.objectId);
                jo.put("type",nowType);
                BmobApi.post(jo, "https://api2.bmob.cn/1/classes/news", new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) { }
                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) { }
                });
                postActivity.finish();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Toast.makeText(postActivity, "请填写完整", Toast.LENGTH_SHORT).show();
        }
    }
    public void setBinding(PostActivity postActivity) {
        PostVM.postActivity =postActivity;
    }
    @BindingAdapter("bindSpinner")
    public static void bindSpinner(Spinner spinner,MutableLiveData<String[]> sp){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(postActivity, android.R.layout.simple_spinner_item, sp.getValue());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                nowType=sp.getValue()[pos];
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent){}
        });
    }
}