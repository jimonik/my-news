package com.mynews.activities.account;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Intent;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.mynews.activities.App;
import com.mynews.activities.admin.AdminActivity;
import com.mynews.databinding.ActivityAccountBinding;
import com.mynews.utils.BmobApi;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
public class AccountVM extends AndroidViewModel {
    ArrayAdapter<String> adp;
    public AccountVM(@NonNull Application application) {
        super(application);
    }
    public void setBinding(ActivityAccountBinding binding, AccountActivity accountActivity) {
        String[] data = new String[]{"我的账号:"+ App.user.username, "我的邮箱:"+ App.user.email, "我的昵称:"+ App.user.nickname, "修改密码","是否管理员："+App.user.isAdmin};
        adp= new ArrayAdapter<>(accountActivity, android.R.layout.simple_list_item_1, data);
        binding.accountList.setAdapter(adp);
        binding.accountList.setOnItemClickListener((parent, view, position, id) -> {
            if (position==2){
                EditText editText=new EditText(accountActivity);
                editText.setHint("请设置昵称");
                AlertDialog.Builder dialog=new AlertDialog.Builder(accountActivity);
                dialog.setTitle("修改昵称").setView(editText).setPositiveButton("确定", (dialog1, which) -> {
                    String str=editText.getText().toString() ;
                    if (!str.equals("")){
                        JSONObject jo=new JSONObject();
                        try {
                            jo.put("nickname",str);
                        }catch(JSONException e){
                            e.printStackTrace();
                        }
                        BmobApi.put(jo, "https://api2.bmob.cn/1/users/" + App.user.objectId,true, new Callback() {
                            @Override
                            public void onFailure(@NotNull Call call, @NotNull IOException e) { accountActivity.runOnUiThread(() -> Toast.makeText(accountActivity, "昵称重置失败", Toast.LENGTH_SHORT).show()); }
                            @Override
                            public void onResponse(@NotNull Call call, @NotNull Response response) {
                                App.user.nickname=str;
                            }
                        });
                        data[position]="我的昵称:"+str;
                    }else {
                        Toast.makeText(accountActivity, "请输入昵称", Toast.LENGTH_SHORT).show();
                    }
                }).setNeutralButton("取消",null).show();
            }
            if (position==3){
                JSONObject jo=new JSONObject();
                try {
                    jo.put("email",App.user.email);
                    BmobApi.post(jo,"https://api2.bmob.cn/1/requestPasswordReset", new Callback(){
                        @Override
                        public void onFailure(@NotNull Call call, @NotNull IOException e) {
                            accountActivity.runOnUiThread(() -> Toast.makeText(accountActivity, "发送改密邮件失败"+e.getMessage(), Toast.LENGTH_SHORT).show());
                        }
                        @Override
                        public void onResponse(@NotNull Call call, @NotNull Response response) {
                            accountActivity.runOnUiThread(() -> Toast.makeText(accountActivity, "发送改密邮件成功",Toast.LENGTH_SHORT).show());
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (position==4&&App.user.isAdmin){
                accountActivity.startActivity(new Intent(accountActivity, AdminActivity.class));
            }
        });
    }
}