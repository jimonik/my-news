package com.mynews.activities.main;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.databinding.BindingAdapter;
import androidx.lifecycle.AndroidViewModel;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mynews.R;
import com.mynews.databinding.ActivityMainBinding;

import org.jetbrains.annotations.NotNull;

public class MainVM extends AndroidViewModel {
    @SuppressLint("StaticFieldLeak")
    private static MainActivity mainActivity;

    public MainVM(@NonNull @NotNull Application application) {
        super(application);
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void setBinding(ActivityMainBinding binding, MainActivity mainActivity) {
        MainVM.mainActivity=mainActivity;
    }
    @BindingAdapter("bindNav")
    public static void bindNav(BottomNavigationView bottomNavigationView, MainVM mainVM){
        NavController navController = Navigation.findNavController(mainActivity, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(mainActivity, navController, new AppBarConfiguration.Builder(R.id.navigation_recommend, R.id.navigation_classification, R.id.navigation_me).build());
        NavigationUI.setupWithNavController(bottomNavigationView, navController);
    }
}
