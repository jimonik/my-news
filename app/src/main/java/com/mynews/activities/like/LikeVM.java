package com.mynews.activities.like;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.mynews.activities.App;
import com.mynews.activities.detail.DetailActivity;
import com.mynews.adapter.NewsAdapter;
import com.mynews.bean.NewsBean;
import com.mynews.databinding.ActivityLikeBinding;
import com.mynews.utils.BmobApi;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class LikeVM extends AndroidViewModel {
    @SuppressLint("StaticFieldLeak")
    private static ActivityLikeBinding binding;
    @SuppressLint("StaticFieldLeak")
    private static LikeActivity likeActivity;
    private final List<NewsBean> data=new ArrayList<>();

    public LikeVM(@NonNull Application application) {
        super(application);
    }
    public void setBinding(ActivityLikeBinding binding, LikeActivity likeActivity) {
        LikeVM.binding=binding;
        LikeVM.likeActivity=likeActivity;
        binding.setAdp(new NewsAdapter(likeActivity,data));
        binding.collectionList.setOnItemClickListener((parent, view, position, id) -> {
            App.newsBean=data.get(position);
            likeActivity.startActivity(new Intent(likeActivity,DetailActivity.class));
        });
        binding.collectionList.setOnItemLongClickListener((parent, view, position, id) -> {
            AlertDialog.Builder dialog=new AlertDialog.Builder(likeActivity);
            dialog.setTitle("提示").setMessage("是否取消赞？").setPositiveButton("确定", (dialog1, which) -> BmobApi.delete("https://api2.bmob.cn/1/classes/like/"+data.get(position).type, new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) { }
                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) {
                    refresh();
                }
            })).setNeutralButton("取消",null).show();

            return true;
        });
        refresh();
    }
    private void refresh() {
        data.clear();
        BmobApi.get("https://api2.bmob.cn/1/classes/like?where={\"userId\":\""+ App.user.objectId+"\"}", new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) { }
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try {
                    assert response.body() != null;
                    JSONArray ja = new JSONObject(response.body().string()).getJSONArray("results");
                    for (int i = 0; i < ja.length(); i++){
                        JSONObject jo=ja.getJSONObject(i);
                        data.add(new NewsBean(jo.getString("newsId"),"","","","",jo.getString("objectId"),null));//此处type放like ID
                    }
                    likeActivity.runOnUiThread(() -> binding.getAdp().notifyDataSetChanged());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
