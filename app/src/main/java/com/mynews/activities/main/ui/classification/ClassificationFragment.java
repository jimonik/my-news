package com.mynews.activities.main.ui.classification;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.library.baseAdapters.BR;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.mynews.R;
import com.mynews.databinding.FragmentClassificationBinding;

public class ClassificationFragment extends Fragment {
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_classification, container, false);
        FragmentClassificationBinding binding=DataBindingUtil.bind(root);
        ClassificationVM vm = new ViewModelProvider(this).get(ClassificationVM.class);
        binding.setVariable(BR.vm, vm);
        binding.setLifecycleOwner(this);
        vm.setBinding(binding, requireActivity());
        return root;
    }

}