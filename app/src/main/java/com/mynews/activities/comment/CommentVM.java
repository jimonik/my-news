package com.mynews.activities.comment;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import com.mynews.activities.App;
import com.mynews.activities.detail.DetailActivity;
import com.mynews.adapter.CommentAdp;
import com.mynews.bean.CommentBean;
import com.mynews.bean.NewsBean;
import com.mynews.databinding.ActivityCommentBinding;
import com.mynews.utils.BmobApi;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
public class CommentVM extends AndroidViewModel {
    @SuppressLint("StaticFieldLeak")
    private static CommentActivity commentActivity;
    private final List<CommentBean> data=new ArrayList<>();
    @SuppressLint("StaticFieldLeak")
    private static ActivityCommentBinding binding;
    public CommentVM(@NonNull Application application) {super(application);}
    public void setBinding(ActivityCommentBinding binding, CommentActivity commentActivity) {
        CommentVM.binding =binding;
        CommentVM.commentActivity=commentActivity;
        binding.setAdp(new CommentAdp(commentActivity, data));
        binding.commentList.setOnItemClickListener((parent, view, position, id) -> {
            CommentBean bean = data.get(position);
            BmobApi.get("https://api2.bmob.cn/1/classes/news/"+bean.ifReplyCommentId, new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) { }
                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                    try {
                        assert response.body() != null;
                        JSONObject jo = new JSONObject(response.body().string());
                        App.newsBean=new NewsBean(jo.getString("objectId"),jo.getString("title"),jo.getString("content"),jo.getString("ownerId"),jo.getString("updatedAt"),jo.getString("type"),App.user);
                        commentActivity.startActivity(new Intent(commentActivity, DetailActivity.class));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        });
        binding.commentList.setOnItemLongClickListener((parent, view, position, id) -> {
            AlertDialog.Builder dialog=new AlertDialog.Builder(commentActivity);
            dialog.setTitle("提示").setMessage("是否删除").setPositiveButton("删除", (dialog1, which) -> {
                CommentBean bean = data.get(position);
                //删除评论下的所有回复
                BmobApi.get("https://api2.bmob.cn/1/classes/reply?where={\"commentId\":\""+ bean.id+"\"}", new Callback() {
                    @Override public void onFailure(@NotNull Call call, @NotNull IOException e) { }
                    @Override public void onResponse(@NotNull Call call, @NotNull Response response) {
                        try {
                            JSONArray ja = new JSONObject(response.body().string()).getJSONArray("results");
                            for (int i = 0; i < ja.length(); i++) {
                                BmobApi.delete("https://api2.bmob.cn/1/classes/reply/"+ja.getJSONObject(i).getString("objectId"), new Callback() {
                                    @Override public void onFailure(@NotNull Call call,@NotNull  IOException e) {
                                    }
                                    @Override public void onResponse(@NotNull Call call,@NotNull  Response response) {
                                    }
                                });
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                //删除评论
                BmobApi.delete("https://api2.bmob.cn/1/classes/comment/"+bean.id, new Callback() {
                    @Override public void onFailure(@NotNull Call call, @NotNull IOException e) { }
                    @Override public void onResponse(@NotNull Call call, @NotNull Response response) {
                        refresh();
                    }
                });
            }).setNeutralButton("取消",null).show();
            return true;
        });
        refresh();
    }
    private void refresh() {
        data.clear();
        BmobApi.get("https://api2.bmob.cn/1/classes/comment?where={\"commentOwnerId\":\""+ App.user.objectId+"\"}", new Callback() {
            @Override public void onFailure(@NotNull Call call, @NotNull IOException e) { }
            @Override public void onResponse(@NotNull Call call, @NotNull Response response) {
                try {
                    JSONArray ja=new JSONObject(response.body().string()).getJSONArray("results");
                    for (int i = 0; i < ja.length(); i++) {
                        JSONObject jo = ja.getJSONObject(i);
                        data.add(new CommentBean(true,jo.getString("objectId"),App.user.objectId,jo.getString("content"),jo.getString("newsId")));
                    }
                    commentActivity.runOnUiThread(() -> binding.getAdp().notifyDataSetChanged());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
